import React from 'react';
import { appContext, AppContextType } from '../AppContext';

export const useAppContext = (): AppContextType => React.useContext(appContext);
