export type Characteristics = {
	viewDistance: number;
	health: number;
	attack: number;
	defence: number;
	agility: number;
};
