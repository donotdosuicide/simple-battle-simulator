import { ETerrain } from '../enums/ETerrain';

type OptionalTerrainFields = Partial<Record<keyof typeof ETerrain, number>>;
type RequiredTerrainFields = Required<Pick<OptionalTerrainFields, 'Plain'>>;

export type TerrainDictionary = OptionalTerrainFields & RequiredTerrainFields;
