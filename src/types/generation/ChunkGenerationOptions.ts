type StartInfo = {
	yStart: number;
	xStart: number;
};

type LengthLimiterInfo = {
	yLengthLimiter?: number;
	xLengthLimiter?: number;
};

type ConditionInfo = {
	yCondition: number;
	xCondition: number;
};

export type ChunkFillingInfo = StartInfo & ConditionInfo & LengthLimiterInfo;
