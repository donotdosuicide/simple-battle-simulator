import { BaseSequenceSizeGenerator } from '../../map/SequenceSizeGenerator/BaseSequenceSizeGenerator';
import { BaseArrayItemDeliverer } from '../../data/ArrayItemDeliverer/BaseArrayItemDeliverer';
import { GameMap } from '../../data/observable/GameMap';
import { ChunkFillingInfo } from './ChunkGenerationOptions';
import { TerrainDictionary } from './TerrainDictionary';

export type MapGenerationOptions = {
	gameMap: GameMap;
	chunkFillingOptions: ChunkFillingInfo[];
	enableChunkFillingOptionsShuffle: boolean;
	terrainQuantity: TerrainDictionary;
	sequenceSizeGeneratorsDeliverer: BaseArrayItemDeliverer<BaseSequenceSizeGenerator>;
};
