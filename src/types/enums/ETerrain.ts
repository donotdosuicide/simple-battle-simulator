export enum ETerrain {
	Plain,
	Forest,
	Hills,
	Mountains,
	Water,
	Dust,
}
