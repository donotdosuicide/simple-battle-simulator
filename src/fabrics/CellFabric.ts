import { ETerrain } from '../types/enums/ETerrain';
import { Coordinate } from '../types/Coordinate';
import { MapCell } from '../data/observable/MapCell';

export class CellFabric {
	public static createCellByType(l: Coordinate, t: ETerrain = ETerrain.Plain): MapCell {
		const cell = new MapCell(l);
		cell.setTerrain(t);
		return cell;
	}
}
