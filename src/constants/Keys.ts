import { EDirection } from '../types/enums/EDirection';

export const NumPadKeys = {
	100: EDirection['←'],
	104: EDirection['↑'],
	102: EDirection['→'],
	98: EDirection['↓'],

	103: EDirection['↖'],
	105: EDirection['↗'],
	97: EDirection['↙'],
	99: EDirection['↘'],
};
