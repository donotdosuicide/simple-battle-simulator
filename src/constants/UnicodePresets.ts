export const empty = '';

export const shades = ['░', '▒'];

export const lines = ['𝄛', '𝄚', '𝄙'];

export const tildes = ['~', '~~'];

export const glissando = ['𝆲', '𝆱'];
