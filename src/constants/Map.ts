export const DEFAULT_MAP_LENGTH = 60;
export const DEFAULT_MAP_WIDTH = 30;

export const DEFAULT_CHUNK_LENGTH = 10;
export const DEFAULT_CHUNK_WIDTH = 10;

export const SQUARE_GENERATION_MIN_PERCENT = 0.05;
export const SQUARE_GENERATION_MAX_PERCENT = 0.3;

export const SIDE_GENERATION_MAX_PERCENT = 0.5;

export const DEFAULT_RATIO_WEIGHTS = {
	Plain: 1,
	Forest: 0.5,
	Hills: 0.1,
	Mountains: 0,
	Water: 0,
	Dust: 0,
};

export const DEFAULT_PERCENT_WEIGHTS = {
	Plain: 0.45,
	Forest: 0.2,
	Hills: 0.05,
	Mountains: 0.05,
	Water: 0.25,
	Dust: 0,
};
