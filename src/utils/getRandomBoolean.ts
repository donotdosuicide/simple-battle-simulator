import { getRandomIntArbitrary } from './getRandomIntArbitrary';

export const getRandomBoolean = (n = 100): boolean => getRandomIntArbitrary(-n, n) > 0;
