export function isValueDefined(value: unknown): boolean {
	return value !== null && value !== undefined;
}
