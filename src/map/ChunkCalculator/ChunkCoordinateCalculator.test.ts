import { FieldSize } from '../../data/observable/FieldSize';
import { ChunkFillingInfo } from '../../types/generation/ChunkGenerationOptions';
import { ChunkCoordinateCalculator } from './ChunkCoordinateCalculator';

describe('ChunkCoordinateCalculator test', () => {
	const mapSize = new FieldSize({ length: 30, width: 30 });
	const chunkSize = new FieldSize({ length: 15, width: 15 });

	const printChunkStart = (chunk: ChunkFillingInfo): string => `xStart:${chunk.xStart} yStart:${chunk.yStart}`;

	describe('chunk counts', () => {
		it('check 4 chunks', () => {
			mapSize.setSide('length', 30);
			mapSize.setSide('width', 30);
			chunkSize.setSide('length', 15);
			chunkSize.setSide('width', 15);

			const calculator = new ChunkCoordinateCalculator(mapSize, chunkSize);
			expect(calculator.getChunkFillingOptions().length).toEqual(4);
		});

		it('check 9 chunks', () => {
			mapSize.setSide('length', 30);
			mapSize.setSide('width', 30);
			chunkSize.setSide('length', 10);
			chunkSize.setSide('width', 10);

			const calculator = new ChunkCoordinateCalculator(mapSize, chunkSize);
			expect(calculator.getChunkFillingOptions().length).toEqual(9);
		});
	});

	describe('chunk data', () => {
		it('check 4 chunks', () => {
			mapSize.setSide('length', 30);
			mapSize.setSide('width', 30);
			chunkSize.setSide('length', 15);
			chunkSize.setSide('width', 15);

			const calculator = new ChunkCoordinateCalculator(mapSize, chunkSize);
			const chunks = calculator.getChunkFillingOptions();
			expect(printChunkStart(chunks[0])).toEqual('xStart:0 yStart:0');
			expect(printChunkStart(chunks[1])).toEqual('xStart:15 yStart:0');
			expect(printChunkStart(chunks[2])).toEqual('xStart:0 yStart:15');
			expect(printChunkStart(chunks[3])).toEqual('xStart:15 yStart:15');
		});

		it('check 9 chunks', () => {
			mapSize.setSide('length', 30);
			mapSize.setSide('width', 30);
			chunkSize.setSide('length', 10);
			chunkSize.setSide('width', 10);

			const calculator = new ChunkCoordinateCalculator(mapSize, chunkSize);
			const chunks = calculator.getChunkFillingOptions();
			expect(printChunkStart(chunks[0])).toEqual('xStart:0 yStart:0');
			expect(printChunkStart(chunks[1])).toEqual('xStart:10 yStart:0');
			expect(printChunkStart(chunks[2])).toEqual('xStart:20 yStart:0');
			expect(printChunkStart(chunks[3])).toEqual('xStart:0 yStart:10');
			expect(printChunkStart(chunks[4])).toEqual('xStart:10 yStart:10');
			expect(printChunkStart(chunks[5])).toEqual('xStart:20 yStart:10');
			expect(printChunkStart(chunks[6])).toEqual('xStart:0 yStart:20');
			expect(printChunkStart(chunks[7])).toEqual('xStart:10 yStart:20');
			expect(printChunkStart(chunks[8])).toEqual('xStart:20 yStart:20');
		});
	});

	describe('non fit chunks', () => {
		it('check 4 chunks', () => {
			mapSize.setSide('length', 30);
			mapSize.setSide('width', 30);
			chunkSize.setSide('length', 25);
			chunkSize.setSide('width', 25);

			const calculator = new ChunkCoordinateCalculator(mapSize, chunkSize);
			const chunks = calculator.getChunkFillingOptions();
			expect(printChunkStart(chunks[0])).toEqual('xStart:0 yStart:0');
			expect(printChunkStart(chunks[1])).toEqual('xStart:25 yStart:0');
			expect(printChunkStart(chunks[2])).toEqual('xStart:0 yStart:25');
			expect(printChunkStart(chunks[3])).toEqual('xStart:25 yStart:25');
		});
	});
});
