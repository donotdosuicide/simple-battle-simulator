import { FieldSize, Sides } from '../../data/observable/FieldSize';
import { Coordinate } from '../../types/Coordinate';
import { ChunkFillingInfo } from '../../types/generation/ChunkGenerationOptions';

export interface IChunkCoordinateCalculator {
	getChunkFillingOptions(): ChunkFillingInfo[];
}

export class ChunkCoordinateCalculator implements IChunkCoordinateCalculator {
	private _mapSize: FieldSize;
	private _chunkSize: FieldSize;

	constructor(mapSize: FieldSize, chunkSize: FieldSize) {
		this._mapSize = mapSize;
		this._chunkSize = chunkSize;
	}

	public getChunkFillingOptions(): ChunkFillingInfo[] {
		const arr: ChunkFillingInfo[] = [];
		const sides = this._chunkSize.sides;

		this._starterCoordinates.forEach((el) => {
			const yStart = el.y;
			const xStart = el.x;

			const yLengthLimiter = 0; // LengthLimiterOptions
			const xLengthLimiter = 0; // LengthLimiterOptions

			const yCondition = sides.width + yStart - yLengthLimiter;
			const xCondition = sides.length + xStart - xLengthLimiter;

			arr.push({ yStart, xStart, yCondition, xCondition });
		});

		return arr;
	}

	private get _starterCoordinates(): Array<Coordinate> {
		return this._computeChunkStarterCoordinates();
	}

	// may compute !!! lengthLimiter !!! for avoid _isBoundaryOutside usage
	private _computeChunkStarterCoordinates(): Array<Coordinate> {
		const coordinates: Array<Coordinate> = [];
		const chunkSides = this._chunkSize.sides;
		const { chunkFitWidth, chunkFitLength } = this._getChunkFitSidesCount();

		let x = 0,
			y = 0;

		for (let i = 0; i < chunkFitWidth; i++) {
			coordinates.push({ y, x });
			for (let j = 1; j < chunkFitLength; j++) {
				x += chunkSides.length;
				coordinates.push({ y, x });
			}
			y += chunkSides.width;
			x = 0;
		}

		return coordinates;
	}

	private _getChunkFitSidesCount() {
		const chunkFitWidth = this._getChunkFitCountBySide('width');
		const chunkFitLength = this._getChunkFitCountBySide('length');

		return { chunkFitWidth, chunkFitLength };
	}

	private _getChunkFitCountBySide(side: keyof Sides): number {
		return Math.ceil(this._mapSize.sides[side] / this._chunkSize.sides[side]);
	}
}
