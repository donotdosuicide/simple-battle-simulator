import { FieldSize } from '../../data/observable/FieldSize';
import { RatioTerrainWeights } from '../../data/observable/TerrainWeights/RatioTerrainWeights';
import { BaseMapPattern, SubPattern } from './BaseMapPattern';
import { RandomFieldFiller } from '../FieldFiller/RandomFieldFiller';

const FOREST_MAP_LENGTH = 30;
const FOREST_MAP_WIDTH = 30;

const FOREST_CHUNK_LENGTH = 15;
const FOREST_CHUNK_WIDTH = 15;

export class WestForestPattern extends BaseMapPattern {
	protected _mapSize = new FieldSize({ length: FOREST_MAP_LENGTH, width: FOREST_MAP_WIDTH });
	protected _chunkSize = new FieldSize({ length: FOREST_CHUNK_LENGTH, width: FOREST_CHUNK_WIDTH });

	private readonly _rndFieldFiller = new RandomFieldFiller();

	protected get _subPatterns(): SubPattern[] {
		return [
			{
				fieldFiller: this._rndFieldFiller,
				chunkFillingOptions: [
					{ xStart: 0, yStart: 0, xCondition: 15, yCondition: 15 },
					{ xStart: 15, yStart: 15, xCondition: 30, yCondition: 30 },
				],
				terrainWeights: new RatioTerrainWeights({
					Plain: 1,
					Forest: 0.4,
					Hills: 0.2,
					Mountains: 0.05,
					Water: 0.05,
					Dust: 0.02,
				}),
			},
			{
				fieldFiller: this._rndFieldFiller,
				chunkFillingOptions: [
					{ xStart: 0, yStart: 15, xCondition: 15, yCondition: 30 },
					{ xStart: 15, yStart: 0, xCondition: 30, yCondition: 15 },
				],
				terrainWeights: new RatioTerrainWeights({
					Plain: 0.5,
					Forest: 1,
					Hills: 0.3,
					Mountains: 0.03,
					Water: 0.01,
					Dust: 0.01,
				}),
			},
		];
	}
}
