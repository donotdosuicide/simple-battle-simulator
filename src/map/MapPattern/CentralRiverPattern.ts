import { FieldSize } from '../../data/observable/FieldSize';
import { RatioTerrainWeights } from '../../data/observable/TerrainWeights/RatioTerrainWeights';
import { ChunkCoordinateCalculator, IChunkCoordinateCalculator } from '../ChunkCalculator/ChunkCoordinateCalculator';
import { BaseMapPattern, SubPattern } from './BaseMapPattern';
import { RandomFieldFiller } from '../FieldFiller/RandomFieldFiller';

const RIVER_MAP_LENGTH = 60;
const RIVER_MAP_WIDTH = 30;

const RIVER_CHUNK_LENGTH = 10;
const RIVER_CHUNK_WIDTH = 5;

const WATER_WEIGHTS = {
	Plain: 0,
	Forest: 0.01,
	Hills: 0,
	Mountains: 0.02,
	Water: 0.88,
	Dust: 0.1,
};

const OTHER_WEIGHTS = {
	Plain: 1,
	Forest: 0.23,
	Hills: 0.14,
	Mountains: 0.08,
	Water: 0.07,
	Dust: 0.07,
};

export class CentralRiverPattern extends BaseMapPattern {
	protected _mapSize = new FieldSize({ length: RIVER_MAP_LENGTH, width: RIVER_MAP_WIDTH });
	protected _chunkSize = new FieldSize({ length: RIVER_CHUNK_LENGTH, width: RIVER_CHUNK_WIDTH });

	private _chunkCoordinateCalculator: IChunkCoordinateCalculator;

	private readonly _waterWeights = new RatioTerrainWeights(WATER_WEIGHTS);
	private readonly _otherWeightsFirst = new RatioTerrainWeights(OTHER_WEIGHTS);
	private readonly _otherWeightsSecond = new RatioTerrainWeights(OTHER_WEIGHTS);

	private readonly _rndFieldFiller = new RandomFieldFiller();

	constructor() {
		super();

		this._chunkCoordinateCalculator = new ChunkCoordinateCalculator(this._mapSize, this._chunkSize);
	}

	protected get _subPatterns(): SubPattern[] {
		const chunkFillingOptions = this._chunkCoordinateCalculator.getChunkFillingOptions();

		const third = Math.ceil(chunkFillingOptions.length / 3);

		const part1 = chunkFillingOptions.splice(0, third);
		const part2 = chunkFillingOptions.splice(third, third + third);
		const part3 = chunkFillingOptions.splice(-third + third);

		return [
			{
				fieldFiller: this._rndFieldFiller,
				chunkFillingOptions: part1,
				terrainWeights: this._otherWeightsFirst,
			},
			{
				fieldFiller: this._rndFieldFiller,
				chunkFillingOptions: part3,
				terrainWeights: this._waterWeights,
			},
			{
				fieldFiller: this._rndFieldFiller,
				chunkFillingOptions: part2,
				terrainWeights: this._otherWeightsSecond,
			},
		];
	}
}
