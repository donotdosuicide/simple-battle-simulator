import { FieldSize } from '../../data/observable/FieldSize';
import { GameMap } from '../../data/observable/GameMap';
import { BaseTerrainWeights } from '../../data/observable/TerrainWeights/BaseTerrainWeights';
import { ChunkFillingInfo } from '../../types/generation/ChunkGenerationOptions';
import { BaseFieldFiller } from '../FieldFiller/BaseFieldFiller';

export type SubPattern = {
	fieldFiller: BaseFieldFiller;
	chunkFillingOptions: ChunkFillingInfo[];
	terrainWeights: BaseTerrainWeights;
};

export abstract class BaseMapPattern {
	protected abstract _mapSize: FieldSize;
	protected abstract _chunkSize: FieldSize;

	protected abstract get _subPatterns(): SubPattern[];

	public get mapSize(): FieldSize {
		return this._mapSize;
	}

	public get chunkSize(): FieldSize {
		return this._chunkSize;
	}

	public generate(gameMap: GameMap): void {
		this._subPatterns.forEach((sP) => sP.fieldFiller.fill(gameMap, sP.chunkFillingOptions, sP.terrainWeights));
	}
}
