import { DEFAULT_CHUNK_LENGTH, DEFAULT_CHUNK_WIDTH, DEFAULT_MAP_LENGTH, DEFAULT_MAP_WIDTH } from '../../constants/Map';
import { FieldSize } from '../../data/observable/FieldSize';
import { BaseTerrainWeights } from '../../data/observable/TerrainWeights/BaseTerrainWeights';
import { ChunkCoordinateCalculator, IChunkCoordinateCalculator } from '../ChunkCalculator/ChunkCoordinateCalculator';
import { BaseMapPattern, SubPattern } from './BaseMapPattern';
import { RandomFieldFiller } from '../FieldFiller/RandomFieldFiller';

export class CustomRandomObservableMapPattern extends BaseMapPattern {
	protected _mapSize = new FieldSize({ length: DEFAULT_MAP_LENGTH, width: DEFAULT_MAP_WIDTH });
	protected _chunkSize = new FieldSize({ length: DEFAULT_CHUNK_LENGTH, width: DEFAULT_CHUNK_WIDTH });

	private _chunkCoordinateCalculator: IChunkCoordinateCalculator;
	private _controlledTerrainWeights: BaseTerrainWeights[];

	private readonly _rndFieldFiller = new RandomFieldFiller();

	constructor(controlledTerrainWeights: BaseTerrainWeights[]) {
		super();

		this._chunkCoordinateCalculator = new ChunkCoordinateCalculator(this._mapSize, this._chunkSize);
		this._controlledTerrainWeights = controlledTerrainWeights;
	}

	protected get _subPatterns(): SubPattern[] {
		const chunkFillingOptions = this._chunkCoordinateCalculator.getChunkFillingOptions();

		return [
			{
				fieldFiller: this._rndFieldFiller,
				chunkFillingOptions: chunkFillingOptions,
				terrainWeights: this._controlledTerrainWeights[0],
			},
		];
	}
}
