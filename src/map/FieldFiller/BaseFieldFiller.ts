import { ArrayHelper } from '../../helpers/ArrayHelper';
import { BaseArrayItemDeliverer } from '../../data/ArrayItemDeliverer/BaseArrayItemDeliverer';
import { GameMap } from '../../data/observable/GameMap';
import { BaseTerrainWeights } from '../../data/observable/TerrainWeights/BaseTerrainWeights';
import { ChunkFillingInfo } from '../../types/generation/ChunkGenerationOptions';
import { MapGenerationOptions } from '../../types/generation/MapGenerationOptions';
import { BaseMapGenerator } from '../MapGenerator/BaseMapGenerator';
import { BaseSequenceSizeGenerator } from '../SequenceSizeGenerator/BaseSequenceSizeGenerator';

export abstract class BaseFieldFiller {
	protected abstract get _mapGenerators(): BaseMapGenerator[];
	protected abstract get _sequenceGenerators(): BaseSequenceSizeGenerator[];

	protected abstract _mapGeneratorsDeliverer: BaseArrayItemDeliverer<BaseMapGenerator>;
	protected abstract _sequenceSizeGeneratorsDeliverer: BaseArrayItemDeliverer<BaseSequenceSizeGenerator>;

	public fill(gameMap: GameMap, chunkFillingOptions: ChunkFillingInfo[], terrainWeights: BaseTerrainWeights): void {
		const commonSquare = gameMap.chunkSize.square * chunkFillingOptions.length;
		const terrainQuantity = terrainWeights.transformWeightsToQuantity(commonSquare);
		const shuffled = ArrayHelper.shuffleArray(chunkFillingOptions);

		(shuffled || chunkFillingOptions).forEach((o) => {
			const generator = this._mapGeneratorsDeliverer.next();

			const generationOptions: MapGenerationOptions = {
				gameMap: gameMap,
				terrainQuantity: terrainQuantity,
				chunkFillingOptions: [o],
				enableChunkFillingOptionsShuffle: false,
				sequenceSizeGeneratorsDeliverer: this._sequenceSizeGeneratorsDeliverer,
			};

			generator?.updateGameMap(generationOptions);
		});
	}
}
