import { RandomArrayItemDeliverer } from '../../data/ArrayItemDeliverer/RandomArrayItemDeliverer';
import { BaseMapGenerator } from '../MapGenerator/BaseMapGenerator';
import { DiagonalSerialMapGenerator } from '../MapGenerator/serial/DiagonalSerialMapGenerator';
import { HorizontalSerialMapGenerator } from '../MapGenerator/serial/HorizontalSerialMapGenerator';
import { SnakeSerialMapGenerator } from '../MapGenerator/serial/SnakeSerialMapGenerator';
import { VerticalSerialMapGenerator } from '../MapGenerator/serial/VerticalSerialMapGenerator';
import { BaseSequenceSizeGenerator } from '../SequenceSizeGenerator/BaseSequenceSizeGenerator';
import { DotSequenceSizeGenerator } from '../SequenceSizeGenerator/DotSequenceSizeGenerator';
import { FragmentSequenceSizeGenerator } from '../SequenceSizeGenerator/FragmentSequenceSizeGenerator';
import { LineSequenceSizeGenerator } from '../SequenceSizeGenerator/LineSequenceSizeGenerator';
import { ShortLineSequenceSizeGenerator } from '../SequenceSizeGenerator/ShortLineSequenceSizeGenerator';
import { BaseFieldFiller } from './BaseFieldFiller';

export class RandomFieldFiller extends BaseFieldFiller {
	protected _mapGeneratorsDeliverer = new RandomArrayItemDeliverer<BaseMapGenerator>(this._mapGenerators);
	protected _sequenceSizeGeneratorsDeliverer = new RandomArrayItemDeliverer(this._sequenceGenerators);

	protected get _mapGenerators(): BaseMapGenerator[] {
		const horizontal = new HorizontalSerialMapGenerator();
		const reverseHorizontal = new HorizontalSerialMapGenerator(true);
		const vertical = new VerticalSerialMapGenerator();
		const reverseVertical = new VerticalSerialMapGenerator(true);
		const snake = new SnakeSerialMapGenerator();
		const reverseSnake = new SnakeSerialMapGenerator(true);
		const diagonal = new DiagonalSerialMapGenerator();
		const reverseDiagonal = new DiagonalSerialMapGenerator(true);

		return [
			horizontal,
			vertical,
			snake,
			reverseHorizontal,
			reverseVertical,
			reverseSnake,
			diagonal,
			reverseDiagonal,
		];
	}

	protected get _sequenceGenerators(): BaseSequenceSizeGenerator[] {
		const dot = new DotSequenceSizeGenerator();
		const shortLine = new ShortLineSequenceSizeGenerator();
		const line = new LineSequenceSizeGenerator();
		const fragment = new FragmentSequenceSizeGenerator();

		return [dot, shortLine, line, fragment];
	}
}
