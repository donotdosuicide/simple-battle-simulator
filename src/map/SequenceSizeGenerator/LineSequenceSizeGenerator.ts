import { SIDE_GENERATION_MAX_PERCENT } from '../../constants/Map';
import { ArrayHelper } from '../../helpers/ArrayHelper';
import { FieldSize } from '../../data/observable/FieldSize';
import { getRandomIntArbitrary } from '../../utils/getRandomIntArbitrary';
import { BaseSequenceSizeGenerator } from './BaseSequenceSizeGenerator';

export class LineSequenceSizeGenerator extends BaseSequenceSizeGenerator {
	public getRandomSequenceSize(currentCellCount: number, mapSize: FieldSize): number {
		const maxLength = Math.floor(
			ArrayHelper.getRandomArrayElement([mapSize.sides.length, mapSize.sides.width]) *
				SIDE_GENERATION_MAX_PERCENT,
		);
		const max = maxLength > currentCellCount ? currentCellCount : maxLength;

		return getRandomIntArbitrary(Math.floor(max / 3), max);
	}
}
