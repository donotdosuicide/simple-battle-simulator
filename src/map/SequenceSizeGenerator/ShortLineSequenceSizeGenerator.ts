import { getRandomIntArbitrary } from '../../utils/getRandomIntArbitrary';
import { BaseSequenceSizeGenerator } from './BaseSequenceSizeGenerator';

export class ShortLineSequenceSizeGenerator extends BaseSequenceSizeGenerator {
	public getRandomSequenceSize(): number {
		return getRandomIntArbitrary(2, 4);
	}
}
