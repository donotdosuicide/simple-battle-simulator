import { FieldSize } from '../../data/observable/FieldSize';

export abstract class BaseSequenceSizeGenerator {
	public abstract getRandomSequenceSize(currentCellCount: number, size: FieldSize): number;
}
