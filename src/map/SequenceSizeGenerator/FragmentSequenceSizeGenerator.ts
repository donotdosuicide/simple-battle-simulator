import { SQUARE_GENERATION_MAX_PERCENT, SQUARE_GENERATION_MIN_PERCENT } from '../../constants/Map';
import { getRandomIntArbitrary } from '../../utils/getRandomIntArbitrary';
import { BaseSequenceSizeGenerator } from './BaseSequenceSizeGenerator';

export class FragmentSequenceSizeGenerator extends BaseSequenceSizeGenerator {
	public getRandomSequenceSize(currentCellCount: number): number {
		const min = Math.floor(currentCellCount * SQUARE_GENERATION_MIN_PERCENT);
		const max = Math.floor(currentCellCount * SQUARE_GENERATION_MAX_PERCENT);

		return getRandomIntArbitrary(min, max);
	}
}
