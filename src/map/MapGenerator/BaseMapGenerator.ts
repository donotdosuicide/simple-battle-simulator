import { ArrayHelper } from '../../helpers/ArrayHelper';
import { ETerrain } from '../../types/enums/ETerrain';
import { ChunkFillingInfo } from '../../types/generation/ChunkGenerationOptions';
import { MapGenerationOptions } from '../../types/generation/MapGenerationOptions';
import { TerrainDictionary } from '../../types/generation/TerrainDictionary';

export abstract class BaseMapGenerator {
	protected _options: MapGenerationOptions = {} as MapGenerationOptions;

	public updateGameMap(options: MapGenerationOptions): void {
		this._options = options;

		this._chunkFillingOptions.forEach((el) => {
			this._generateChunk(el);
		});
	}

	protected abstract _generateChunk(options: ChunkFillingInfo): void;

	protected get _randomKeyIfValueAvailable(): keyof TerrainDictionary {
		const notEmptyKeys: Array<keyof typeof ETerrain> = [];

		for (const key in this._options.terrainQuantity) {
			const typedKey = key as keyof TerrainDictionary;
			if ((this._options.terrainQuantity[typedKey] as number) > 0) {
				notEmptyKeys.push(typedKey);
			}
		}

		return ArrayHelper.getRandomArrayElement(notEmptyKeys);
	}

	protected _isBoundaryOutside(y: number, x: number): boolean {
		const { width, length } = this._options.gameMap.mapSize.sides;

		return y >= width || x >= length;
	}

	private get _chunkFillingOptions(): Array<ChunkFillingInfo> {
		return this._options.enableChunkFillingOptionsShuffle
			? ArrayHelper.shuffleArray(this._options.chunkFillingOptions)
			: this._options.chunkFillingOptions;
	}
}
