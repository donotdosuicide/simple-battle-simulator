import { CellFabric } from '../../../fabrics/CellFabric';
import { Coordinate } from '../../../types/Coordinate';
import { ETerrain } from '../../../types/enums/ETerrain';
import { ChunkFillingInfo } from '../../../types/generation/ChunkGenerationOptions';
import { TerrainDictionary } from '../../../types/generation/TerrainDictionary';
import { BaseSequenceSizeGenerator } from '../../SequenceSizeGenerator/BaseSequenceSizeGenerator';
import { BaseMapGenerator } from '../BaseMapGenerator';

type GenerationStage = {
	key: keyof TerrainDictionary;
	sequenceSize: number;
};

export abstract class BaseSerialMapGenerator extends BaseMapGenerator {
	private _seqGenerator: BaseSequenceSizeGenerator | undefined;
	private _genStage: GenerationStage | undefined;

	protected _isReverse: boolean;

	constructor(isReverse = false) {
		super();

		this._isReverse = isReverse;
	}

	protected _generateChunk(chunkFillingOptions: ChunkFillingInfo): void {
		this._seqGenerator = this._options.sequenceSizeGeneratorsDeliverer.next();
		this._genStage = { key: 'Plain', sequenceSize: 0 };

		this._fillChunk(chunkFillingOptions);
	}

	protected _fillChunk(options: ChunkFillingInfo): void {
		if (this._isReverse) {
			this._reverseFill(options);
		} else {
			this._standardFill(options);
		}
	}

	protected abstract _standardFill(options: ChunkFillingInfo): void;
	protected abstract _reverseFill(options: ChunkFillingInfo): void;

	protected _fillCellByCoordinate(c: Coordinate): void {
		this._fillCell(c.y, c.x);
	}

	protected _fillCell(y: number, x: number): void {
		if (!this._genStage || this._isBoundaryOutside(y, x)) {
			return;
		}

		if (this._genStage.sequenceSize <= 0 && this._seqGenerator) {
			this._genStage.key = this._randomKeyIfValueAvailable;

			this._genStage.sequenceSize = this._seqGenerator.getRandomSequenceSize(
				this._options.terrainQuantity[this._genStage.key] as number,
				this._options.gameMap.chunkSize,
			);
		}

		const cell = CellFabric.createCellByType({ x, y }, ETerrain[this._genStage.key]);
		this._options.gameMap.cells[y][x] = cell;

		this._options.terrainQuantity[this._genStage.key]--;
		this._genStage.sequenceSize--;
	}
}
