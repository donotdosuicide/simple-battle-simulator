import { ChunkFillingInfo } from '../../../types/generation/ChunkGenerationOptions';
import { BaseSerialMapGenerator } from './BaseSerialMapGenerator';

export class HorizontalSerialMapGenerator extends BaseSerialMapGenerator {
	protected _standardFill(options: ChunkFillingInfo): void {
		for (let y = options.yStart; y < options.yCondition; y++) {
			for (let x = options.xStart; x < options.xCondition; x++) {
				this._fillCell(y, x);
			}
		}
	}

	protected _reverseFill(options: ChunkFillingInfo): void {
		for (let y = options.yCondition - 1; y >= options.yStart; y--) {
			for (let x = options.xCondition - 1; x >= options.xStart; x--) {
				this._fillCell(y, x);
			}
		}
	}
}
