import { ChunkFillingInfo } from '../../../types/generation/ChunkGenerationOptions';
import { BaseSerialMapGenerator } from './BaseSerialMapGenerator';

export class SnakeSerialMapGenerator extends BaseSerialMapGenerator {
	public _standardFill(options: ChunkFillingInfo): void {
		let standardXTraverse = true;
		for (let y = options.yStart; y < options.yCondition; y++) {
			this._snakeXFill(options, standardXTraverse, y);
			standardXTraverse = !standardXTraverse;
		}
	}

	public _reverseFill(options: ChunkFillingInfo): void {
		let standardXTraverse = false;
		for (let y = options.yCondition - 1; y >= options.yStart; y--) {
			this._snakeXFill(options, standardXTraverse, y);
			standardXTraverse = !standardXTraverse;
		}
	}

	private _snakeXFill(options: ChunkFillingInfo, standardXTraverse: boolean, y: number) {
		if (standardXTraverse) {
			for (let x = options.xStart; x < options.xCondition; x++) {
				this._fillCell(y, x);
			}
		} else {
			for (let x = options.xCondition - 1; x >= options.xStart; x--) {
				this._fillCell(y, x);
			}
		}
	}
}
