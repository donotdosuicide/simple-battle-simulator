import { MoveStateMachine } from '../../../data/MoveStateMachine';
import { Coordinate } from '../../../types/Coordinate';
import { EDirection } from '../../../types/enums/EDirection';
import { ChunkFillingInfo } from '../../../types/generation/ChunkGenerationOptions';
import { BaseSerialMapGenerator } from './BaseSerialMapGenerator';

export class DiagonalSerialMapGenerator extends BaseSerialMapGenerator {
	protected _standardFill(opt: ChunkFillingInfo): void {
		const c = { x: opt.xStart, y: opt.yStart };
		this._fillCellByCoordinate(c);

		while (this._isNotUpperBorder(c, opt)) {
			this._moveAndFill(EDirection['→'], c);

			while (c.x > opt.xStart && this._isNotUpperBorder(c, opt)) this._moveAndFill(EDirection['↖'], c);

			if (this._isNotUpperBorder(c, opt)) {
				this._moveAndFill(EDirection['↑'], c);

				while (c.y > opt.yStart && this._isNotUpperBorder(c, opt)) this._moveAndFill(EDirection['↘'], c);
			}
		}

		let isEvenSides = this._isEvenSides(opt);

		while (this._isNotRightBorder(c, opt)) {
			isEvenSides && this._moveAndFill(EDirection['→'], c);

			while (this._isNotRightBorder(c, opt) && c.y !== opt.yStart) this._moveAndFill(EDirection['↘'], c);

			if (this._isNotUpperBorder(c, opt)) {
				c.x === opt.xCondition - 1
					? this._moveAndFill(EDirection['↑'], c)
					: this._moveAndFill(EDirection['→'], c);

				while (this._isNotUpperBorder(c, opt)) this._moveAndFill(EDirection['↖'], c);

				isEvenSides = true;
			}
		}
	}

	protected _reverseFill(opt: ChunkFillingInfo): void {
		const c = { x: opt.xCondition - 1, y: opt.yCondition - 1 };
		this._fillCellByCoordinate(c);

		while (this._isNotDownBorder(c, opt)) {
			this._moveAndFill(EDirection['←'], c);

			while (c.x < opt.xCondition - 1 && this._isNotDownBorder(c, opt)) this._moveAndFill(EDirection['↘'], c);

			if (this._isNotDownBorder(c, opt)) {
				this._moveAndFill(EDirection['↓'], c);

				while (c.y < opt.yCondition - 1 && this._isNotDownBorder(c, opt)) this._moveAndFill(EDirection['↖'], c);
			}
		}

		let isEvenSides = this._isEvenSides(opt);

		while (this._isNotLeftBorder(c, opt)) {
			isEvenSides && this._moveAndFill(EDirection['←'], c);

			while (this._isNotLeftBorder(c, opt) && c.y !== opt.yCondition - 1) this._moveAndFill(EDirection['↖'], c);

			if (this._isNotDownBorder(c, opt)) {
				c.x === opt.xStart ? this._moveAndFill(EDirection['↓'], c) : this._moveAndFill(EDirection['←'], c);

				while (this._isNotDownBorder(c, opt)) this._moveAndFill(EDirection['↘'], c);

				isEvenSides = true;
			}
		}
	}

	private _moveAndFill = (dir: EDirection, c: Coordinate) =>
		MoveStateMachine.move(dir, c, () => this._fillCellByCoordinate(c));

	private _isEvenSides = (opt: ChunkFillingInfo) =>
		(opt.xCondition - opt.xStart) % 2 === 0 && (opt.yCondition - opt.yStart) % 2 === 0;

	private _isNotUpperBorder = (c: Coordinate, options: ChunkFillingInfo) => c.y !== options.yCondition - 1;

	private _isNotRightBorder = (c: Coordinate, options: ChunkFillingInfo) => c.x !== options.xCondition - 1;

	private _isNotDownBorder = (c: Coordinate, options: ChunkFillingInfo) => c.y !== options.yStart;

	private _isNotLeftBorder = (c: Coordinate, options: ChunkFillingInfo) => c.x !== options.xStart;
}
