import { observer } from 'mobx-react';
import { FunctionComponent } from 'react';
import { FogVisibilityHelper } from '../../helpers/FogVisibilityHelper';
import { UnitDistanceHelper } from '../../helpers/UnitDistanceHelper';
import { useAppContext } from '../../hooks/useAppContext';
import { MapCell } from '../../data/observable/MapCell';
import { isValueDefined } from '../../utils/common';
import { MapCellView } from './MapCell/MapCellView';

interface MapRowProps {
	rowIdx: number;
	cells: MapCell[];
	isRowFromViewableRange: boolean;
}

export const MapRow: FunctionComponent<MapRowProps> = observer((props) => {
	const { heroStore, mapSettingStore } = useAppContext();

	const getCellKey = (cell: MapCell): string => `rowIdx: ${cell.coordinate.y} celIdx: ${cell.coordinate.x}`;

	const isCellFromViewableRange = (celIdx: number) =>
		props.isRowFromViewableRange && FogVisibilityHelper.isCellFromViewableRange(celIdx, heroStore.hero);

	const isCellFromHeroCircleDistanceView = (cell: MapCell) =>
		isValueDefined(
			UnitDistanceHelper.getDistanceBetweenCellAndUnitOptimise(
				{ y: cell.coordinate.y, x: cell.coordinate.x },
				heroStore.hero,
			),
		);

	const isCellVisibleInFogMode = (cell: MapCell) =>
		mapSettingStore.isFogEnabledFlag.isEnabled &&
		isCellFromViewableRange(cell.coordinate.x) &&
		isCellFromHeroCircleDistanceView(cell);

	const exploreAreaIfAvailable = (cell: MapCell): void => {
		if (!cell.isExploredArea && isCellVisibleInFogMode(cell)) {
			cell.setIsExploredArea(true);
		}
	};

	return (
		<div className='map-row'>
			{props.cells.map((cell) => {
				exploreAreaIfAvailable(cell);
				return (
					<MapCellView
						key={getCellKey(cell)}
						celldata={cell}
						isVisibleInFogMode={isCellVisibleInFogMode(cell)}
					/>
				);
			})}
		</div>
	);
});
