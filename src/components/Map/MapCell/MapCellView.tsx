import classNames from 'classnames';
import { observer } from 'mobx-react';
import { FunctionComponent, useMemo } from 'react';
import { MapCell } from '../../../data/observable/MapCell';
import { BaseUnit } from '../../../data/Unit/BaseUnit';
import { MapCellRandomContentHelper } from '../../../helpers/MapCellRandomContentHelper';
import { useAppContext } from '../../../hooks/useAppContext';
import { ETeam } from '../../../types/enums/ETeam';
import { ETerrain } from '../../../types/enums/ETerrain';
import { isValueDefined } from '../../../utils/common';

interface MapCellViewProps {
	celldata: MapCell;
	isVisibleInFogMode?: boolean;
}

export const MapCellView: FunctionComponent<MapCellViewProps> = observer((props) => {
	const { heroStore, mapSettingStore } = useAppContext();

	const unit = props.celldata.unit;
	const terrain = props.celldata.terrain;
	const isHeroCell = unit?.id === heroStore.hero.id;
	const isAdvancedView = mapSettingStore.isAdvancedViewFlag.isEnabled;

	const randomBackgroundColor = useMemo(
		() => MapCellRandomContentHelper.getRandomBackgroundColor(terrain),
		[terrain],
	);
	const randomTerrain = useMemo(
		() => MapCellRandomContentHelper.getRandomTerrain(terrain, isAdvancedView),
		[terrain, isAdvancedView],
	);

	const onClickCell = () => {
		console.log(props.celldata);
		console.log(`Coordinate: y: ${props.celldata.coordinate.y} x: ${props.celldata.coordinate.x}`);

		isHeroCell && alert('Its hero!!!');
	};

	const getCellClassName = () => {
		const unitColorClass = unit && classNames('map-cell_unit', `map-cell_unit-team_${ETeam[unit.team]}`);
		const cellVisibleClass = props.isVisibleInFogMode ? 'map-cell_visible' : undefined;
		const cellExploredClass =
			mapSettingStore.isFogEnabledFlag.isEnabled && props.celldata.isExploredArea && !props.isVisibleInFogMode
				? 'map-cell_explored'
				: undefined;

		return classNames(
			'map-cell',
			`map-cell_terrain_${ETerrain[terrain]}`,
			unitColorClass,
			cellExploredClass,
			cellVisibleClass,
		);
	};

	const showUnit = (unit: BaseUnit): string => {
		return unit.icon;
	};

	const styles = {
		backgroundColor: isValueDefined(terrain) ? randomBackgroundColor : undefined,
	};

	return (
		<div style={styles} onClick={onClickCell} className={getCellClassName()}>
			{unit ? showUnit(unit) : randomTerrain}
		</div>
	);
});
