import classNames from 'classnames';
import { observer } from 'mobx-react';
import { FunctionComponent } from 'react';
import { useAppContext } from '../../hooks/useAppContext';
import { MapRow } from './MapRow';
import { FogVisibilityHelper } from '../../helpers/FogVisibilityHelper';

export const Map: FunctionComponent = observer(() => {
	const { gameStore, mapSettingStore, heroStore } = useAppContext();

	return (
		<div
			className={classNames(
				'map-field',
				mapSettingStore.isColumnReverseFlag.isEnabled ? 'map-field_reverse' : undefined,
				mapSettingStore.isFogEnabledFlag.isEnabled ? 'map-field_use-fog' : undefined,
			)}
		>
			{gameStore.gameMap.cells?.map((row, rowIdx) => (
				<MapRow
					key={'rowIdx:' + rowIdx}
					rowIdx={rowIdx}
					cells={row}
					isRowFromViewableRange={FogVisibilityHelper.isRowFromViewableRange(rowIdx, heroStore.hero)}
				/>
			))}
		</div>
	);
});
