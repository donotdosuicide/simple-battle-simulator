import { observer } from 'mobx-react';
import React, { FunctionComponent } from 'react';
import { useAppContext } from '../../../../hooks/useAppContext';

interface IMapButtonsBarProps {
	onGenerate: () => void;
}

export const MapButtonsBar: FunctionComponent<IMapButtonsBarProps> = observer((props) => {
	const { gameStore, mapSettingStore, levelStore } = useAppContext();

	return (
		<div>
			<button onClick={() => levelStore.initBaseUnitPreset()} type='button'>
				Add units
			</button>

			<button onClick={() => mapSettingStore.isAdvancedViewFlag.toggle()} type='button'>
				{mapSettingStore.isAdvancedViewFlag.isEnabled ? 'Use simple view' : 'Use advanced view'}
			</button>

			<button onClick={() => mapSettingStore.isColumnReverseFlag.toggle()} type='button'>
				{mapSettingStore.isColumnReverseFlag.isEnabled ? 'Disable col reverse' : 'Enable col reverse'}
			</button>

			<button onClick={() => mapSettingStore.isFogEnabledFlag.toggle()} type='button'>
				{mapSettingStore.isFogEnabledFlag.isEnabled ? 'Disable fog' : 'Enable fog'}
			</button>

			<button onClick={() => gameStore.gameMap.equalizeChunkToMap()} type='button'>
				Equalize chunk to map
			</button>

			<button onClick={props.onGenerate} type='button'>
				Regenerate
			</button>
		</div>
	);
});
