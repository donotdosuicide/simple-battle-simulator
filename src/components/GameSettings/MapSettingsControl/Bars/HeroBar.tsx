import { observer } from 'mobx-react';
import React, { FunctionComponent } from 'react';
import { useAppContext } from '../../../../hooks/useAppContext';

export const HeroBar: FunctionComponent = observer(() => {
	const { heroStore } = useAppContext();

	return (
		<div className='herobar'>
			<span>Hero preset: {heroStore.hero?.icon}</span>
			<span>
				Hero coords: {heroStore.hero?.coordinate.x} {heroStore.hero?.coordinate.y}
			</span>
			<span>Hero view distance: {heroStore.hero?.characteristics.viewDistance}</span>
		</div>
	);
});
