import { observer } from 'mobx-react';
import { Fragment, FunctionComponent } from 'react';
import { useAppContext } from '../../../../hooks/useAppContext';
import { Sides } from '../../../../data/observable/FieldSize';

export const MapSizeBar: FunctionComponent = observer(() => {
	const { gameStore } = useAppContext();
	const { mapSize, chunkSize } = gameStore.gameMap;

	const fields = [
		{ name: 'map', field: mapSize },
		{ name: 'chunk', field: chunkSize },
	];
	const sides: Array<keyof Sides> = ['length', 'width'];

	return (
		<div className='sizebar'>
			<hr />
			{fields.map((el) => (
				<div key={el.name}>
					{sides.map((sideKey) => {
						const sideStr = sideKey.toString();
						const id = `${el.name}_${sideStr}`;
						return (
							<Fragment key={id}>
								<label htmlFor={id}>{id}</label>
								<input
									id={id}
									onChange={(e) => el.field.setSide(sideKey, +e.target.value)}
									value={el.field.sides[sideKey]}
									type='number'
								/>
								<br />
							</Fragment>
						);
					})}
					<hr />
				</div>
			))}
		</div>
	);
});
