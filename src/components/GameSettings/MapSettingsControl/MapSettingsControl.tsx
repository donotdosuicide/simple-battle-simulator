import { observer } from 'mobx-react';
import { FunctionComponent, useEffect } from 'react';
import { useAppContext } from '../../../hooks/useAppContext';
import { GenerationSettingsControl } from '../GenerationSettingsControl/GenerationSettingsControl';
import { HeroBar } from './Bars/HeroBar';
import { MapButtonsBar } from './Bars/MapButtonsBar';
import { MapSizeBar } from './Bars/MapSizeBar';
import './MapSettingsControl.scss';

export const MapSettingsControl: FunctionComponent = observer(() => {
	const { gameStore, unitStore } = useAppContext();

	const generate = (): void => {
		gameStore.gameMap.generate();
		unitStore.updateGameMapUnits();
	};

	useEffect(generate, [
		gameStore,
		gameStore.gameMap.mapSize.sides.length,
		gameStore.gameMap.mapSize.sides.width,
		gameStore.gameMap.chunkSize.sides.length,
		gameStore.gameMap.chunkSize.sides.width,
		unitStore,
	]);

	return (
		<div className='control-panel'>
			<div className='row'>
				<MapSizeBar />
				<GenerationSettingsControl />
				<HeroBar />
			</div>
			<div className='row'>
				<MapButtonsBar onGenerate={generate} />
			</div>
		</div>
	);
});
