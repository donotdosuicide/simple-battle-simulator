import { observer } from 'mobx-react';
import React, { FunctionComponent } from 'react';
import { useAppContext } from '../../../hooks/useAppContext';
import { TerrainDictionary } from '../../../types/generation/TerrainDictionary';
import './GenerationSettingsControl.scss';

export const GenerationSettingsControl: FunctionComponent = observer(() => {
	const { gameStore } = useAppContext();

	const terrainWeights = gameStore.terrainWeights;
	const keys = terrainWeights.dictionaryKeys;

	const onRatioRangeChange = (key: keyof TerrainDictionary, val: number): void => {
		terrainWeights.setWeightByKey(key, val);
	};

	const onRatioRangeMouseUp = (): void => {
		gameStore.gameMap.generate();
	};

	return (
		<div className='generation-settings'>
			<div>
				{keys.map((key) => {
					return (
						<div className='weight-control' key={key}>
							<label htmlFor={key}>{key}</label>
							<input
								type='range'
								id={key}
								name={key}
								min='0'
								max='1'
								step='0.01'
								value={terrainWeights.dictionary[key]}
								onChange={(e) => onRatioRangeChange(key, +e.target.value)}
								onMouseUp={() => onRatioRangeMouseUp()}
							/>
							<span>{terrainWeights.dictionary[key]}</span>
						</div>
					);
				})}
			</div>

			{/* <div className='generation-options__symbol'>&#125;</div> */}
			{/* <span className='generation-options__common-sum'>{terrainWeights.commonSum.toFixed(2)}</span> */}
			{/* <pre id='json'>{JSON.stringify(mapSettingStore.terrainWeights.dictionary, undefined, 4)}</pre> */}
		</div>
	);
});
