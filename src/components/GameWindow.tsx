import { observer } from 'mobx-react';
import { FunctionComponent, useEffect, useRef } from 'react';
import { NumPadKeys } from '../constants/Keys';
import { useAppContext } from '../hooks/useAppContext';
import { EDirection } from '../types/enums/EDirection';
import { isValueDefined } from '../utils/common';
import { MapSettingsControl } from './GameSettings/MapSettingsControl/MapSettingsControl';
import { Map } from './Map/Map';

export const GameWindow: FunctionComponent = observer(() => {
	const { heroStore } = useAppContext();
	const gameRef = useRef<HTMLDivElement>(null);

	useEffect(() => {
		gameRef.current?.focus();
		heroStore.addHero();
	}, [heroStore]);

	const onKeyDown = (keyCode: number) => {
		const dirs: { [key: number]: EDirection } = {
			37: EDirection['←'],
			38: EDirection['↑'],
			39: EDirection['→'],
			40: EDirection['↓'],

			...NumPadKeys,
		};

		const dir = dirs[keyCode];
		isValueDefined(dir) && heroStore.moveHero(dir);
	};

	return (
		<div ref={gameRef} tabIndex={0} onKeyDown={(e) => onKeyDown(e.keyCode)} className='game'>
			<MapSettingsControl />
			<Map />
		</div>
	);
});
