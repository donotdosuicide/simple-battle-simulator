import { Container } from 'inversify';
/*  !!! reflect-metadata should be imported before any interface or other imports
    also it should be imported only once so that a singleton is created !!! */
import 'reflect-metadata';
import { NotificationService } from '../services/NotificationService';
import { Types } from './inversify.types';

const container = new Container();

container.bind<NotificationService>(Types.NotificationService).to(NotificationService).inSingletonScope();

export default container;
