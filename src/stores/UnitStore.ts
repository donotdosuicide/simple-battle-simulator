import { GameMap } from '../data/observable/GameMap';
import { BaseUnit } from '../data/Unit/BaseUnit';
import { EDirection } from '../types/enums/EDirection';

export class UnitStore {
	private readonly _gameMap: GameMap;
	private _units: Array<BaseUnit> = [];

	constructor(map: GameMap) {
		this._gameMap = map;
	}

	public get units(): Array<BaseUnit> {
		return this._units;
	}

	public moveUnit(unitIdx: number, dir: EDirection): void {
		this._units[unitIdx].move(dir);

		this.updateGameMapUnits();
	}

	public addUnitToGame(unit: BaseUnit, disableUpdate = true, uniqueIdx?: number): void {
		this._addUnit(unit, uniqueIdx);
		this._gameMap.placeUnitToMap(unit);

		if (disableUpdate) {
			return;
		}

		this.updateGameMapUnits();
	}

	public updateGameMapUnits(): void {
		for (let y = 0; y < this._gameMap.mapSize.sides.width; y++) {
			for (let x = 0; x < this._gameMap.mapSize.sides.length; x++) {
				this._setUnitByAvailability(y, x);
			}
		}
	}

	public defineTarget(): void {
		this._units.forEach((u) => {
			const enemies = this._getAllEnemyFor(u);
			u.findNearestEnemy(enemies);
		});
	}

	private _addUnit(unit: BaseUnit, uniqueIdx?: number): void {
		if (uniqueIdx !== undefined) {
			this._units[uniqueIdx] = unit;
		} else {
			this._units.push(unit);
		}
	}

	private _setUnitByAvailability(y: number, x: number): void {
		const unit = this._units.find((u) => u.coordinate.x === x && u.coordinate.y === y);
		this._gameMap.cells[y][x].setUnit(unit);
	}

	private _getAllEnemyFor(unit: BaseUnit): BaseUnit[] {
		return this._units.filter((u) => !u.isFriendFor(unit));
	}
}
