import { computed, makeObservable, observable } from 'mobx';
import { DEFAULT_RATIO_WEIGHTS } from '../constants/Map';
import { GameMap } from '../data/observable/GameMap';
import { BaseTerrainWeights } from '../data/observable/TerrainWeights/BaseTerrainWeights';
import { RatioTerrainWeights } from '../data/observable/TerrainWeights/RatioTerrainWeights';
import { BaseMapPattern } from '../map/MapPattern/BaseMapPattern';
import { CentralRiverPattern } from '../map/MapPattern/CentralRiverPattern';
import { CustomRandomObservableMapPattern } from '../map/MapPattern/CustomRandomObservableMapPattern';
import { WestForestPattern } from '../map/MapPattern/WestForestPattern';

export class GameStore {
	@observable private _gameMap: GameMap;
	@observable private _terrainWeights: BaseTerrainWeights;

	private _defaultPattern: BaseMapPattern;
	private _centralRiverPattern: BaseMapPattern;
	private _westForestPattern: BaseMapPattern;

	constructor() {
		makeObservable(this);

		this._terrainWeights = new RatioTerrainWeights(DEFAULT_RATIO_WEIGHTS);

		this._defaultPattern = new CustomRandomObservableMapPattern([this._terrainWeights]);
		this._centralRiverPattern = new CentralRiverPattern();
		this._westForestPattern = new WestForestPattern();

		this._gameMap = new GameMap(this._defaultPattern);
	}

	@computed
	public get gameMap(): GameMap {
		return this._gameMap;
	}

	@computed
	public get terrainWeights(): BaseTerrainWeights {
		return this._terrainWeights;
	}
}
