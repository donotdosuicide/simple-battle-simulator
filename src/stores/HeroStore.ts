import { action, computed, makeObservable, observable } from 'mobx';
import { GameMap } from '../data/observable/GameMap';
import { BaseUnit } from '../data/Unit/BaseUnit';
import { FootWarrior } from '../data/Unit/FootWarrior';
import { Coordinate } from '../types/Coordinate';
import { EDirection } from '../types/enums/EDirection';
import { ETeam } from '../types/enums/ETeam';
import { UnitStore } from './UnitStore';

export class HeroStore {
	private readonly _gameMap: GameMap;
	private readonly _unitStore: UnitStore;

	private readonly _heroIdx = 0;

	@observable private _hero: BaseUnit;
	@observable private _heroCoordinates: Coordinate;

	constructor(gameMap: GameMap, unitStore: UnitStore) {
		makeObservable(this);

		this._gameMap = gameMap;
		this._unitStore = unitStore;

		this._heroCoordinates = { x: 0, y: this._gameMap.mapSize.sides.width - 1 };
		this._hero = new FootWarrior(ETeam.Blue, this._heroCoordinates);
	}

	@computed
	public get hero(): BaseUnit {
		return this._hero;
	}

	@action
	public addHero(): void {
		this._unitStore.addUnitToGame(this._hero, false, this._heroIdx);
	}

	@action
	public moveHero(dir: EDirection): void {
		if (!this._hero) {
			return;
		}

		this._unitStore.moveUnit(this._heroIdx, dir);
	}
}
