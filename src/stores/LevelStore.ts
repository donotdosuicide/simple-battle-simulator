import { GameMap } from '../data/observable/GameMap';
import { FootWarrior } from '../data/Unit/FootWarrior';
import { ETeam } from '../types/enums/ETeam';
import { UnitStore } from './UnitStore';

export class LevelStore {
	private readonly _gameMap: GameMap;
	private readonly _unitStore: UnitStore;

	constructor(gameMap: GameMap, unitStore: UnitStore) {
		this._gameMap = gameMap;
		this._unitStore = unitStore;
	}

	public initBaseUnitPreset(): void {
		const map = this._gameMap;
		const unitCount = Math.floor(map.mapSize.sides.length / 4);
		const xStart = Math.floor((map.mapSize.sides.length - unitCount) / 2);

		for (let y = 0; y < map.mapSize.sides.width; y++) {
			for (let x = 0; x < map.mapSize.sides.length; x++) {
				const isInitUnitPosition = x >= xStart && x <= xStart + unitCount;
				const isLastRow = y === map.mapSize.sides.width - 1;

				if (y === 0 && isInitUnitPosition) {
					const redFootWarrior = new FootWarrior(ETeam.Red, { x, y });
					this._unitStore.addUnitToGame(redFootWarrior);
				} else if (isLastRow && isInitUnitPosition) {
					const blueFootWarrior = new FootWarrior(ETeam.Blue, { x, y });
					this._unitStore.addUnitToGame(blueFootWarrior);
				}
			}
		}
		this._unitStore.updateGameMapUnits();
	}
}
