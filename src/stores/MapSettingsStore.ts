import { makeObservable, observable } from 'mobx';
import { ObservedFlag } from '../data/observable/ObservedFlag';

export class MapSettingsStore {
	@observable public readonly isAdvancedViewFlag = new ObservedFlag(true);
	@observable public readonly isColumnReverseFlag = new ObservedFlag(true);
	@observable public readonly isFogEnabledFlag = new ObservedFlag(false);

	constructor() {
		makeObservable(this);
	}
}
