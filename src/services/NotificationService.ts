import { injectable } from 'inversify';

type ConsoleNotificationKey = Extract<keyof Console, 'warn' | 'error'>;
type NotificationType = ConsoleNotificationKey;

@injectable()
export class NotificationService {
	private readonly notifier = console;

	public static readonly errorNotificationType = 'error';
	public static readonly warningNotificationType = 'warn';

	public createNotification(type: NotificationType, text: string): void {
		this.notifier[type](text);
	}
}
