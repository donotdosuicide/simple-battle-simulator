import { observer } from 'mobx-react';
import { ReactElement } from 'react';
import './App.scss';
import { GameWindow } from './components/GameWindow';

const App = observer((): ReactElement => {
	return (
		<div className='App'>
			<GameWindow />
		</div>
	);
});

export default App;
