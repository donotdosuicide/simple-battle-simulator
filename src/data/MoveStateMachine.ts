import { Coordinate } from '../types/Coordinate';
import { EDirection } from '../types/enums/EDirection';

export class MoveStateMachine {
	public static move(dir: EDirection, coordinate: Coordinate, callback?: () => void): void {
		switch (dir) {
			case EDirection['↑']:
				coordinate.y++;
				break;
			case EDirection['←']:
				coordinate.x--;
				break;
			case EDirection['↓']:
				coordinate.y--;
				break;
			case EDirection['→']:
				coordinate.x++;
				break;

			case EDirection['↖']:
				coordinate.y++;
				coordinate.x--;
				break;
			case EDirection['↗']:
				coordinate.y++;
				coordinate.x++;
				break;
			case EDirection['↙']:
				coordinate.y--;
				coordinate.x--;
				break;
			case EDirection['↘']:
				coordinate.y--;
				coordinate.x++;
				break;

			default:
				console.error('Incorrect direction!');
				break;
		}

		callback && callback();
	}
}
