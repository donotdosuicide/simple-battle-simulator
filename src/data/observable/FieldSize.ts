import { action, computed, makeObservable, observable } from 'mobx';

export type Sides = {
	length: number;
	width: number;
};
export class FieldSize {
	@observable private _sides: Sides;

	constructor(s: Sides) {
		makeObservable(this);

		this._sides = s;
	}

	@computed
	public get sides(): Sides {
		return this._sides;
	}

	@action
	public setSide(key: keyof Sides, value: number): void {
		this._sides[key] = value;
	}

	@computed
	public get square(): number {
		return this._sides.length * this._sides.width;
	}
}
