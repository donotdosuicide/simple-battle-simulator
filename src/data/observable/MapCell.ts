import { action, computed, makeObservable, observable } from 'mobx';
import { Coordinate } from '../../types/Coordinate';
import { ETerrain } from '../../types/enums/ETerrain';
import { BaseUnit } from '../Unit/BaseUnit';

export class MapCell {
	@observable private _unit: BaseUnit | undefined = undefined;
	@observable private _isExploredArea: boolean;

	private _terrain: ETerrain;
	private _coordinate: Coordinate;

	constructor(coordinate: Coordinate, terrain = ETerrain.Plain) {
		makeObservable(this);

		this._coordinate = coordinate;
		this._terrain = terrain;
		this._isExploredArea = false;
	}

	@computed
	public get unit(): BaseUnit | undefined {
		return this._unit;
	}

	@action
	public setUnit(u: BaseUnit | undefined): void {
		this._unit = u;
	}

	@computed
	public get isExploredArea(): boolean {
		return this._isExploredArea;
	}

	@action
	public setIsExploredArea(val: boolean): void {
		this._isExploredArea = val;
	}

	public get terrain(): ETerrain {
		return this._terrain;
	}

	public setTerrain(t: ETerrain): void {
		this._terrain = t;
	}

	public get coordinate(): Coordinate {
		return this._coordinate;
	}
}
