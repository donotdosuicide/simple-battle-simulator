import { TerrainDictionary } from '../../../types/generation/TerrainDictionary';
import { BaseTerrainWeights } from './BaseTerrainWeights';

export class RatioTerrainWeights extends BaseTerrainWeights {
	/*  
        Example: 
        1x + 1x + 0.5x + 0.75x + 0x = 900 || 100%
	    3.25x = 900 || 100%
	    x = 276.92 || 30.77%
    */
	public transformWeightsToQuantity(mapSquare: number): TerrainDictionary {
		const x = this._getProportionValueByMapSquare(mapSquare);

		const transformedWeights: TerrainDictionary = { Plain: 0 };
		this.dictionaryKeys.forEach((key) => {
			const ratioValue = this.dictionary[key];
			transformedWeights[key] = ratioValue ? Math.floor(ratioValue * x) : 0;
		});

		return transformedWeights;
	}

	private _getProportionValueByMapSquare(mapSquare: number): number {
		let xСoeff = 0;

		this.dictionaryKeys.forEach((key) => {
			const ratioValue = this.dictionary[key];
			xСoeff += ratioValue ? ratioValue : 0;
		});

		return mapSquare / xСoeff;
	}
}
