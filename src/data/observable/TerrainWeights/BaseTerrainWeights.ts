import { action, computed, makeObservable, observable } from 'mobx';
import { ArrayHelper } from '../../../helpers/ArrayHelper';
import { TerrainDictionary } from '../../../types/generation/TerrainDictionary';

export abstract class BaseTerrainWeights {
	@observable private _weightDictionary: TerrainDictionary;

	constructor(initDict: TerrainDictionary) {
		makeObservable(this);

		this._weightDictionary = initDict;
	}

	public abstract transformWeightsToQuantity(mapSquare: number): TerrainDictionary;

	@computed
	public get dictionary(): TerrainDictionary {
		return this._weightDictionary;
	}

	@computed
	public get dictionaryKeys(): Array<keyof TerrainDictionary> {
		return Object.keys(this._weightDictionary) as Array<keyof TerrainDictionary>;
	}

	@action
	public setWeightByKey(key: keyof TerrainDictionary, value: number): void {
		this._weightDictionary[key] = value;
	}

	@computed
	public get commonSum(): number {
		return ArrayHelper.reduceArray(Object.values(this._weightDictionary) as number[]);
	}
}
