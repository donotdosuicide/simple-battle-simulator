import { DEFAULT_PERCENT_WEIGHTS } from '../../../constants/Map';
import { TerrainDictionary } from '../../../types/generation/TerrainDictionary';
import { BaseTerrainWeights } from './BaseTerrainWeights';

// need to align values after input ranges changes
export class PercentTerrainWeights extends BaseTerrainWeights {
	public transformWeightsToQuantity(mapSquare: number): TerrainDictionary {
		const transformedWeights: TerrainDictionary = { Plain: 0 };
		this.dictionaryKeys.forEach((key) => {
			const percentValue = this.dictionary[key];
			transformedWeights[key] = percentValue ? Math.floor(percentValue * mapSquare) : 0;
		});

		return transformedWeights;
	}

	private _validatePercentWeights(percentWeights: TerrainDictionary): TerrainDictionary {
		if (this.commonSum !== 1) {
			const textError =
				'Sum of weights should be equal 1 (100%). Weights object was changed to default ({ Plain: 1 }) !!!';
			console.error(textError);
			alert(textError);

			percentWeights = DEFAULT_PERCENT_WEIGHTS;
		}

		return percentWeights;
	}
}
