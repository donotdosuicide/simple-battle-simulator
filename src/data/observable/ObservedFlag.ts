import { action, computed, makeObservable, observable } from 'mobx';

export class ObservedFlag {
	@observable private _flag;

	constructor(initial: boolean) {
		makeObservable(this);
		this._flag = initial;
	}

	@computed public get isEnabled(): boolean {
		return this._flag;
	}

	@action public toggle(): void {
		this._flag = !this._flag;
	}
}
