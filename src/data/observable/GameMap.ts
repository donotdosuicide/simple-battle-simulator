import { action, computed, makeObservable, observable } from 'mobx';
import { BaseMapPattern } from '../../map/MapPattern/BaseMapPattern';
import { BaseUnit } from '../Unit/BaseUnit';
import { FieldSize } from './FieldSize';
import { MapCell } from './MapCell';

export class GameMap {
	@observable private _mapSize: FieldSize;
	@observable private _chunkSize: FieldSize;
	@observable private _cells: Array<Array<MapCell>> = [];

	private _pattern: BaseMapPattern;

	constructor(pattern: BaseMapPattern) {
		makeObservable(this);

		this._pattern = pattern;
		this._mapSize = pattern.mapSize;
		this._chunkSize = pattern.chunkSize;
	}

	@action
	public rebuildByObservableSizes(): void {
		this._cells = [];
		for (let y = 0; y < this._mapSize.sides.width; y++) {
			this._cells[y] = new Array<MapCell>(this._mapSize.sides.length);
		}
	}

	public setPattern(p: BaseMapPattern): void {
		this._pattern = p;
		this._mapSize = p.mapSize;
		this._chunkSize = p.chunkSize;
	}

	@computed
	public get mapSize(): FieldSize {
		return this._mapSize;
	}

	@computed
	public get chunkSize(): FieldSize {
		return this._chunkSize;
	}

	@computed
	public get cells(): Array<Array<MapCell>> {
		return this._cells;
	}

	@action
	public placeUnitToMap(unit: BaseUnit): void {
		const { y, x } = unit.coordinate;
		this._cells[y][x].setUnit(unit);
	}

	@action
	public equalizeChunkToMap(): void {
		this._chunkSize.setSide('length', this._mapSize.sides.length);
		this._chunkSize.setSide('width', this._mapSize.sides.width);
	}

	public generate(): void {
		this.rebuildByObservableSizes();
		this._pattern.generate(this);
	}
}
