import { Characteristics } from '../../types/Characteristics';
import { Coordinate } from '../../types/Coordinate';
import { ETeam } from '../../types/enums/ETeam';
import { BaseUnit } from './BaseUnit';

export class FootWarrior extends BaseUnit {
	protected _coordinate: Coordinate;
	protected _characteristics: Characteristics;
	protected _team: ETeam;
	protected _viewIcon: string;

	constructor(team: ETeam, coordinate = { x: 0, y: 0 }) {
		super();

		this._coordinate = coordinate;
		this._team = team;
		this._viewIcon = '♟'; // '♖' '♘'

		this._characteristics = {
			health: 150,
			attack: 10,
			defence: 10,
			agility: 8,
			viewDistance: 4,
		};
	}
}
