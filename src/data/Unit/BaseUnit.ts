import { v4 } from 'uuid';
import { Characteristics } from '../../types/Characteristics';
import { Coordinate } from '../../types/Coordinate';
import { EDirection } from '../../types/enums/EDirection';
import { ETeam } from '../../types/enums/ETeam';
import { MoveStateMachine } from '../MoveStateMachine';

export abstract class BaseUnit {
	private _id = v4();

	protected abstract _coordinate: Coordinate;
	protected abstract _characteristics: Characteristics;
	protected abstract _team: ETeam;
	protected abstract _viewIcon: string;

	public move(dir: EDirection): void {
		const prevCoordinate = { ...this._coordinate };

		MoveStateMachine.move(dir, this._coordinate);

		console.log(
			`Unit move from { x:${prevCoordinate.x} y:${prevCoordinate.y} } to { x:${this._coordinate.x} y:${this._coordinate.y}}`,
		);
	}

	public get id(): string {
		return this._id;
	}

	public get icon(): string {
		return this._viewIcon;
	}

	public get team(): ETeam {
		return this._team;
	}

	public get coordinate(): Coordinate {
		return this._coordinate;
	}

	public get characteristics(): Characteristics {
		return this._characteristics;
	}

	public isFriendFor(unit: BaseUnit): boolean {
		return this._team === unit._team;
	}

	public findNearestEnemy(enemies: BaseUnit[]): void {
		enemies.forEach((e) => e.coordinate);
	}
}
