import { BaseArrayItemDeliverer } from './BaseArrayItemDeliverer';

export class AlternationArrayItemDeliverer<T> extends BaseArrayItemDeliverer<T> {
	protected _gettingRule(): T | undefined {
		return this._alternation();
	}

	private _alternation(): T {
		const elem = this._array[this._currentIdx];
		this._currentIdx++;

		if (this._currentIdx === this._array.length) {
			this._currentIdx = 0;
		}

		return elem;
	}
}
