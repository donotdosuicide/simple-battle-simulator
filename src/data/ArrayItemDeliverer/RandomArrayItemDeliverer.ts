import { ArrayHelper } from '../../helpers/ArrayHelper';
import { BaseArrayItemDeliverer } from './BaseArrayItemDeliverer';

export class RandomArrayItemDeliverer<T> extends BaseArrayItemDeliverer<T> {
	protected _gettingRule(): T | undefined {
		return ArrayHelper.getRandomArrayElement(this._array);
	}
}
