export abstract class BaseArrayItemDeliverer<T> {
	protected _array: T[];
	protected _currentIdx = 0;

	constructor(array: T[]) {
		this._array = array;
	}

	protected abstract _gettingRule(): T | undefined;

	public next(): T | undefined {
		if (!this._array.length) {
			return;
		}

		if (this._array.length === 1) {
			return this._array[0];
		}

		return this._gettingRule();
	}
}
