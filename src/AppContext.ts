import React from 'react';
import { HeroStore } from './stores/HeroStore';
import { LevelStore } from './stores/LevelStore';
import { UnitStore } from './stores/UnitStore';
import { GameStore } from './stores/GameStore';
import { MapSettingsStore } from './stores/MapSettingsStore';

const gameStore = new GameStore();
const mapSettingStore = new MapSettingsStore();
const unitStore = new UnitStore(gameStore.gameMap);
const heroStore = new HeroStore(gameStore.gameMap, unitStore);
const levelStore = new LevelStore(gameStore.gameMap, unitStore);

const stores = {
	gameStore,
	mapSettingStore,
	unitStore,
	heroStore,
	levelStore,
};

const objectContext = {
	...stores,
};

export const appContext = React.createContext(objectContext);

export type AppContextType = typeof objectContext;
