import { empty, glissando, lines, shades, tildes } from '../constants/UnicodePresets';
import { ETerrain } from '../types/enums/ETerrain';
import { ArrayHelper } from './ArrayHelper';

export class MapCellRandomContentHelper {
	private static readonly backgroundColors: Record<ETerrain, Array<string>> = {
		[ETerrain.Plain]: ['#2ecc40', '#77dd77', '#03c03c', '#40d450'],
		[ETerrain.Dust]: ['#d1ae87', '#ffd685', '#ffca79', '#ffdead'],
		[ETerrain.Forest]: ['#157c4e', '#157c25', '#0a874f', '#1b7725'],
		[ETerrain.Hills]: ['#0c573b', '#0e5133', '#0a3b25', '#0e5033'],
		[ETerrain.Mountains]: ['#816c5b', '#2c251f', '#56483d', '#362d26'],
		[ETerrain.Water]: ['#7fdbff', '#b2ffff', '#a5ffff', '#afeeee'],
	};

	private static readonly defaultCellSymbols: Record<ETerrain, string> = {
		[ETerrain.Forest]: 'F',
		[ETerrain.Plain]: 'P',
		[ETerrain.Mountains]: 'M',
		[ETerrain.Hills]: 'H',
		[ETerrain.Water]: 'W',
		[ETerrain.Dust]: 'D',
	};

	private static readonly extendedCellSymbols: Record<ETerrain, Array<string>> = {
		[ETerrain.Forest]: ['🌳', '🌳', '🌲', '🌿', '🍃', empty],
		[ETerrain.Plain]: [...lines, ...shades, '🌾', empty, '🌼', ...lines, ...shades, '🌱'],
		[ETerrain.Hills]: [...shades, ...glissando, ...lines, empty],
		[ETerrain.Mountains]: ['🗻', ...glissando, ...glissando, empty],
		[ETerrain.Water]: [...tildes, ...tildes, '🌊', empty, ...tildes, ...tildes, '🐟', empty],
		[ETerrain.Dust]: [...shades, empty],
	};

	public static getRandomBackgroundColor(t: ETerrain): string {
		return ArrayHelper.getRandomArrayElement(this.backgroundColors[t]);
	}

	public static getRandomTerrain(t: ETerrain, isAdvancedView: boolean): string {
		return isAdvancedView
			? ArrayHelper.getRandomArrayElement(this.extendedCellSymbols[t])
			: this.defaultCellSymbols[t];
	}
}
