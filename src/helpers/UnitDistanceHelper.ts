import { BaseUnit } from '../data/Unit/BaseUnit';
import { Coordinate } from '../types/Coordinate';

export class UnitDistanceHelper {
	public static getDistanceBetweenCellAndUnitOptimise = (
		cellCoordinate: Coordinate,
		unit: BaseUnit,
	): number | undefined => {
		const aSide = Math.abs(unit.coordinate.x - cellCoordinate.x);
		const bSide = Math.abs(unit.coordinate.y - cellCoordinate.y);
		const distance = Math.floor(Math.sqrt(aSide ** 2 + bSide ** 2));

		if (
			aSide > unit.characteristics.viewDistance ||
			bSide > unit.characteristics.viewDistance ||
			distance > unit.characteristics.viewDistance
		) {
			return;
		}

		return distance;
	};
}
