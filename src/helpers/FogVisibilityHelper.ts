import { BaseUnit } from '../data/Unit/BaseUnit';
import { Coordinate } from '../types/Coordinate';
import { UnitDistanceHelper } from './UnitDistanceHelper';

export class FogVisibilityHelper {
	public static isRowFromViewableRange(rowIdx: number, hero: BaseUnit): boolean {
		return FogVisibilityHelper.isDimensionFromViewableRange('y', rowIdx, hero);
	}

	public static isCellFromViewableRange(celIdx: number, hero: BaseUnit): boolean {
		return FogVisibilityHelper.isDimensionFromViewableRange('x', celIdx, hero);
	}

	public static getCellDetailedOpacityByHeroInfo(cellCoordinate: Coordinate, hero: BaseUnit): number | undefined {
		const unitRemoteness = UnitDistanceHelper.getDistanceBetweenCellAndUnitOptimise(cellCoordinate, hero);
		return unitRemoteness !== undefined
			? FogVisibilityHelper.getCellOpacityByDistanceData(unitRemoteness, hero.characteristics.viewDistance)
			: undefined;
	}

	private static isDimensionFromViewableRange(dim: keyof Coordinate, idx: number, hero: BaseUnit): boolean {
		return Math.abs(idx - hero.coordinate[dim]) <= hero.characteristics.viewDistance;
	}

	private static getCellOpacityByDistanceData(unitRemoteness: number, unitViewDistance?: number): number {
		// unitViewDistance for value align

		if (unitRemoteness <= 2) {
			return 1;
		} else if (unitRemoteness > 2 && unitRemoteness <= 5) {
			return 0.9;
		} else if (unitRemoteness > 5 && unitRemoteness <= 8) {
			return 0.8;
		}

		return 1.45 - +`0.${unitRemoteness}`;
	}
}
