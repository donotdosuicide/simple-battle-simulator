import { getRandomIntArbitrary } from '../utils/getRandomIntArbitrary';

export class ArrayHelper {
	public static getRandomArrayElement<T>(array: T[]): T {
		return array[getRandomIntArbitrary(0, array.length)];
	}

	public static reduceArray(array: number[]): number {
		return array.reduce((acc, current) => acc + current, 0);
	}

	public static shuffleArray<T>(array: T[]): T[] {
		let currentIndex = array.length,
			temporaryValue,
			randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}
}
