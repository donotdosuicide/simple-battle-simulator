export class EnumHelper {
	public static getKeysOfEnum(enumObject: Record<string, unknown>): string[] {
		return Object.keys(enumObject).filter((item) => isNaN(Number(item)));
	}
}
